import Head from "next/head";
import { NextPage } from "next";
import { Container, FormElement, Input, Link, Text } from "@nextui-org/react";
import { io } from "socket.io-client";
import React, { InputHTMLAttributes, useEffect, useState } from "react";
import { Socket } from "socket.io-client";

let socket: Socket;

const Home: NextPage = () => {
  const [input, setInput] = useState("");
  useEffect(() => {
    (async () => {
      await fetch("/api/socket");
      socket = io();

      socket.on("update-input", (msg) => {
        setInput(msg);
      });
    })();
  });

  const onChangeHandler = (e: React.ChangeEvent<FormElement>) => {
    setInput(e.target.value);
    socket.emit("input-change", e.target.value);
  };

  return (
    <>
      <Head>
        <title>Socket.IO + Next.js Test</title>
        <meta name="description" content="Socket.IO + Next.js Test" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container css={{ maxWidth: 800 }}>
        <Text h1>Socket.IO + Next.js Test</Text>
        <Text>
          This is a demo of using{" "}
          <Link css={{ display: "inline" }} href="https://socket.io/">
            Socket.IO
          </Link>{" "}
          +{" "}
          <Link css={{ display: "inline" }} href="https://nextjs.org/">
            Next.js
          </Link>{" "}
          +{" "}
          <Link
            css={{ display: "inline" }}
            href="https://www.typescriptlang.org/"
          >
            TypeScript
          </Link>{" "}
          +{" "}
          <Link css={{ display: "inline" }} href="https://nextui.org/">
            NextUI
          </Link>
          . Simply type a message in the input box and see it change the value
          across all browser sessions.
        </Text>
        <br />
        <br />
        <Input
          aria-label="Message"
          labelPlaceholder="Message"
          value={input}
          onChange={onChangeHandler}
        />
        <br />
        <br />

        <Text h3>References:</Text>
        <ul>
          <li>
            <Link href="https://blog.logrocket.com/implementing-websocket-communication-next-js/">
              https://blog.logrocket.com/implementing-websocket-communication-next-js/
            </Link>
          </li>
          <li>
            <Link href="https://github.com/igdev116/instagram-noob">
              https://github.com/igdev116/instagram-noob
            </Link>
          </li>
        </ul>
      </Container>
    </>
  );
};

export default Home;
