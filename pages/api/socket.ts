import { NextApiRequest, NextApiResponse } from "next";
import { Server } from "socket.io";
import { Socket as NetSocket } from "net";
import { Server as HttpServer } from "http";

type NextApiResponseSocketIO = NextApiResponse & {
  socket: NetSocket & {
    server: HttpServer & {
      io: Server;
    };
  };
};

const socketHandler = (_req: NextApiRequest, res: NextApiResponseSocketIO) => {
  if (res.socket.server.io) {
    console.log("Socket already created.");
  } else {
    const io = new Server(res.socket.server);
    res.socket.server.io = io;

    io.on("connection", (socket) => {
      socket.on("input-change", (msg) => {
        socket.broadcast.emit("update-input", msg);
      });
    });
  }
  res.end();
};

export default socketHandler;
